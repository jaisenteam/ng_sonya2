# SONYA Angular 1.6

## Installation
* Clone the repo:

	```git clone https://bitbucket.org/jaisenteam/sonya-angular```

* Go to root directory:

	```cd sonya-angular```

* Install dependencies:

	```npm i```
	
* Start server:

	``` npm run lite```
	
	
## Routing (ngRoute)

Inside app.js file:

	.config(function($routeProvider,$locationProvider) {
	
		$routeProvider
			.when('/path1', {
    			templateUrl: 'views/path1.html'
    		})
    		.when('/path2', {
    			templateUrl: 'views/path2.html'	
    		})
        
    	$locationProvider.html5Mode(true);
    });
    
Full example [here](https://docs.angularjs.org/api/ngRoute/service/$route#example)