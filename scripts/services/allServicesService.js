/**
 * Created by vohcan on 28/3/17.
 */
angular.module('sonya').service('AllServicesService', function($http,Properties) {
    this.getServices = function () {

        return $http.get(Properties.allServices);
    };
    this.getServicesDetail = function (serviceId) {

        if(serviceId) {


            return $http.get(Properties.endPointBase + "service/" + serviceId);
        } else {

            return $http.get(Properties.allServices);

        }
    };

    this.getServicesFromSubcategoryId = function(id){
        return $http.get(Properties.allServices+'/'+id);
    };

    this.photoAbsolutePath = function (relativePath) {

        //Este es el metodo corto de hacer uun if, es decir si existe rutaRelativa hacemos
        //Http//localhost.....+ rutaRelativa. Si no undefined


     return relativePath ? (Properties.imageUrl +"/"+ relativePath) : undefined;

    };
});
