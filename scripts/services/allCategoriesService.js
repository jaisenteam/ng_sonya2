/**
 * Created by vohcan on 26/3/17.
 */
angular.module('sonya').service('AllCategoriesService', function($http,Properties) {
    this.getServiceFilter = function(){
        // return $http.get(Properties.endPointBase + "servicefilter");
        return $http.get(Properties.urlServicesFilter);
    };
    this.getAllCategories = function(){
        return $http.get(Properties.endPointBase + 'Category');
    };
    this.getSubCategory = function(){
        return $http.get(Properties.endPointBase+ 'subcategory');
    }
    this.getSubCategoryFromId = function(id){
        return $http.get(Properties.endPointBase+ 'subcategory/'+id);
    };
});
