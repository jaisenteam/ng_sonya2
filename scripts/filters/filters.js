/**
 * Created by vohcan on 28/3/17.
 */
angular.module('sonya')
    .filter('sanitize', ['$sce', function($sce) {
        return function(htmlCode){
            //Con esto devolvemos codigo HTML bien formateado a la vista
            return $sce.trustAsHtml(htmlCode);

        };
    }]);