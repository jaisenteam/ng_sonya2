/**
 * Created by vohcan on 26/3/17.
 */
angular
    .module('sonya')
    .value('Properties', {
        endPointBase: 'https://1qh4lbakmd.execute-api.eu-west-1.amazonaws.com/prod/v1/',
        urlServicesFilter: 'https://1qh4lbakmd.execute-api.eu-west-1.amazonaws.com/prod/v1/servicefilter',
        imageUrl: 'http://d11633325zmxa2.cloudfront.net',
        allServices: "https://1qh4lbakmd.execute-api.eu-west-1.amazonaws.com/prod/v1/getAllServices"
});
