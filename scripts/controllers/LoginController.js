/**
 * Created by juan_arillo on 3/3/17.
 */

angular.module("sonya").controller("LoginController", function ($scope) {

    $scope.login = function () {
        var poolData = {
            UserPoolId : 'eu-west-1_BVfouDsex',
            ClientId : '5vld69uimi8q79im405g595s9'
        };
        var userPool =
            new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
        var userData = {
            Username : document.getElementById("nickname").value,
            Pool : userPool
        };

        var authenticationData = {
            Username : document.getElementById("nickname").value,
            Password : document.getElementById("password").value
        };
        var authenticationDetails =
            new AWSCognito.CognitoIdentityServiceProvider.AuthenticationDetails(authenticationData);

        var cognitoUser =
            new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);
        cognitoUser.authenticateUser(authenticationDetails, {
            onSuccess: function (result) {
                console.log('access token + ' + result.getAccessToken().getJwtToken());
            },

            onFailure: function(err) {
                alert(err);
            },
            mfaRequired: function(codeDeliveryDetails) {
                var verificationCode = prompt('Please input verification code' ,'');
                cognitoUser.sendMFACode(verificationCode, this);
            }
        });

    }

});

