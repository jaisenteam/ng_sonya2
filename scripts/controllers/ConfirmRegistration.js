/**
 * Created by juan_arillo on 2/3/17.
 */

angular.module("sonya").controller("ConfirmRegistration", function ($scope,$location) {

    $scope.confReg = function () {

        var poolData = {
            UserPoolId : 'eu-west-1_BVfouDsex',
            ClientId : '5vld69uimi8q79im405g595s9'
        };

        var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);

        var userData = {
            Username : document.getElementById("nick").value,
            Pool : userPool
        };

        var cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);

        var conf = document.getElementById("confirm").value;

        function confirmation(callback) {
            cognitoUser.confirmRegistration(conf, true, function(err, result) {
                if (err) {
                    alert(err);
                    return;
                }
                console.log('call result: ' + result);
            });

            callback()

        }

        confirmation(function () {
            $location.path('/Login');
        });

    }


});
