/**
 * Created by vohcan on 28/3/17.
 */
angular
    .module('sonya')
    .component('allServices', {
        templateUrl: 'views/allServices.html',
        controller: function (AllServicesService) {


            var self = this;

            //Con el oninit para que se actualice cada vez que lo llamamos
            self.$onInit = function () {

                AllServicesService.getServices().then(function (response) {

                    self.services = response.data.data;

                    console.log(self.services);
                });

            }

            self.absolutePath = AllServicesService.photoAbsolutePath;

        }

});
