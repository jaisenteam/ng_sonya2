/**
 * Created by vohcan on 26/3/17.
 */
angular.module('sonya').component('root', {

    templateUrl: 'views/root.html',
    $routeConfig: [
        {
            name:'AllServices',
            path:'/allservices',
            component:'allServices',
            useAsDefault: true
        },
        {
            name: "ServiceDetail",
            path: "/allservices/:id",
            component: "serviceDetail"

        },
        {
            name:'ServicesFilter',
            path:'/servicesfilter',
            component:'servicesFilter'
        },
        {
            name:'Categories',
            path:'/categories',
            component:'categories'
            // useAsDefault: true
        },
        {
            name:'SubCategories',
            path:'/subcategories',
            component:'subCategories'

        },
        {
            name:'UserConsole',
            path:'/userconsole',
            component:'userConsole'

        }
    ]

});
