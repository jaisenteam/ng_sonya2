/**
 * Created by vohcan on 27/3/17.
 */
angular
    .module('sonya')
    .component('subCategories', {
        templateUrl: 'views/subCategories.html',
        controller: function(AllCategoriesService){
            var self = this;

            self.$routerOnActivate = function(infoFromCategoryView){

                var cat = infoFromCategoryView.params.id;
                console.log(cat);

                AllCategoriesService.getSubCategoryFromId(cat).then(function(res){
                    self.subCategories = res.data.data;
                    console.log(self.subCategories);

                });

            };


        },
        bindings:{
            id: '=',
            name: '='
        }

});