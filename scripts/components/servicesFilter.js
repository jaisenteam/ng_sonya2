/**
 * Created by vohcan on 28/3/17.
 */
angular.module('sonya').component('servicesFilter', {
    templateUrl: 'views/servicesFilter.html',
    controller: function(AllServicesService){
        var self = this;

        self.$routerOnActivate = function(infoFromCategoryView){

            var serv = infoFromCategoryView.params.id;
            console.log(serv);

            AllServicesService.getServicesFromSubcategoryId(serv).then(function(res){
                self.serviceFiltr = res.data.data;
                console.log(self.serviceFiltr);

            });
            self.absolutePath = AllServicesService.photoAbsolutePath;

        };


    },
});
