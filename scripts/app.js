/**
 * Created by vohcan on 26/3/17.
 */

angular.module('sonya',['ngComponentRouter']);

//config $locationProvider
angular.module('sonya').config(function ($locationProvider) {
    $locationProvider.html5Mode(true);

});

angular.module('sonya').value('$routerRootComponent', 'root','userConsole');




