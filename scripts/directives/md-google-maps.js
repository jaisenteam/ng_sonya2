/**
 * Created by vohcan on 31/3/17.
 */


/*
    angular.module('sonya')
        .directive('mdGoogleMaps',function(){
            return {
                restrict: 'E',
                replace: true,
                scope: {
                    lat:'=',
                    lng:'='
                },
                template: '<div id="md-google-maps"></div>',
                link: function(scope, element, attrs) {



                    var center = new google.maps.LatLng(scope.lat,scope.lng);
                    var zoom = scope.zoom || 12;
                    var mapSettings = {
                        center: center,
                        zoom: zoom
                    };

                    var map = {};
                    addMarker();


                    initialize();

                    function addMarker(){
                        var arrMarkers = new google.maps.Marker({position: center});
                    }

                    function initialize() {
                        map = new google.maps.Map(element[0], mapSettings);
                    }

                }
            }
        } );


*/
angular
    .module('sonya')
    .directive('mdGoogleMaps',function(){
        var self = this;

        return {
            restrict: 'AE',
            replace: true,
            scope:{
                lat:'@',
                lng:'@'
            },
            templateUrl: 'views/md-google-maps.html',
            link: function(scope, element, attrs){
                //  var latitude = self.service.latitude;
                // var longitude = self.service.longitude;
                var center = new google.maps.LatLng(scope.lat,scope.lng);
                console.log(center);
                var zoom = 15;
                var mapSettings = {
                    center: center,
                    zoom: zoom
                };

                initialize();

                function initialize(){
                    map = new google.maps.Map(element[0], mapSettings);
                    var pin = new google.maps.Marker({
                        position: center,
                        map: map
                    });
                }
            }
        }


});


