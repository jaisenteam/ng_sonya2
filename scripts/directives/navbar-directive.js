
angular.module("sonya").directive("navbarDirective", function () {

    //La barra de navegacion en una vista por si queremos hacer algo con ella
    return {

        restrict: "EA",
        templateUrl: "views/navbar-view.html"

    };
});
